package com.papalam.actt.classifier

data class Result(val result: String, val confidence: Float)